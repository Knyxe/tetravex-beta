open Graph_tetra
open Grid_tetra
open Parse_tetra
open Graphics

let int_of_bool b = if b then 1 else 0

let rec start_tetra () =
	ignore( wait_next_event [Button_down] );
	if button_down () && grid_clicked () then begin
		let g = which_grid_clicked () in
			update_piece g.pieces g;
			drag g;
			draw_screen ()
	end
	else
		if screen_resized () then
			draw_screen ();
	start_tetra ()

let () =
	intGrid.gx <- !side_span + !cell_size * !grid_size + !mid_span;
	intGrid.gx <- !bot_span;
	finGrid.gx <- !side_span;
	finGrid.gy <- !bot_span;
	
	if Array.length Sys.argv = 1 then
		Printf.printf "Argument manquant\n"
	else
	(***********************************)
		let path = Sys.argv.(1) in
			if parse_file path then begin
				init path;
				let dim = Printf.sprintf " %ix%i" !right !up in
					open_graph dim;
					set_window_title "Tetravex";
					auto_synchronize false;
					scale_lengths ();
					draw_screen ();
					try
						start_tetra ()
					with
					| _ -> Printf.printf "------------\nBye :)\n------------\n"
			end
			else
				Printf.printf "%s" !error_msg
	(***********************************)