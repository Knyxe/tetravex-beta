(*------------------------------------------------------------*)
type piece = {name: string; x: int; y: int}
type 'a tree = L of bool | N of 'a tree * 'a * 'a tree

let accept m =
	Printf.printf "%s dans la case (%i,%i)\n" m.name m.x m.y

let rec truc noeud not_found found () =
	match noeud with
	| L(b) -> if b then found () else not_found ()
	| N(g, m, d) -> truc g
						(truc d
							not_found (*When not found in the tree d*)
							(fun () -> accept m;found ()) (*When found in the tree d*)
						) (*When not found in the tree g*)
						(fun () -> accept m; found ()) (*When found in the tree g*)
						() (*Execute the function*)

let init_tree () =
	N(
		N(
			L(false),
			{name="a0"; x=0; y=0},
			N(
				L(false),
				{name="a1"; x=1; y=0},
				L(false)
			)
		),

		{name="racine"; x=2; y=0},
		
		N(
			L(true),
			{name="a3"; x=3; y=0},
			N(
				L(true),
				{name="a4"; x=4; y=0},
				L(false)
			)
		)
	)

let () =
	let t = init_tree () in
		match t with
			| N(g, m, d) -> truc g (truc d ignore (fun () -> accept m)) (fun () -> accept m) ()
			| L(b) -> ()