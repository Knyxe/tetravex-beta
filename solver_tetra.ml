open Grid_tetra
open Parse_tetra

type xleaf = X of int * int * piece
type neleaf = NE of int * int (*not empty*)

type bdd = 	Empty
				| X of xleaf * bdd * bdd
				| NE of  neleaf * bdd * bdd
				| N of piece list * bdd * bdd
				| T (*True*)
				| F (*False*)

(* Construire l'arbre depuis les feuilles
	coller la racine de chaque arbre avec les feuilles V des arbres supérieurs:
		if isValid(dernièreFeuille) then dernièreFeuille <- racine(arbreVerif)
	avec arbreVerif = N(piece, arbreG, arbreD) par exemple
	*)

let build_bdd l bdd =
	List.fold_left (fun p bdd -> ())
