open Graph_tetra
open Grid_tetra

let error_msg = ref "Fichier invalide:\n"

let debug = false

let int_of_char' char =
	int_of_char char - (int_of_char '0')

let read_char chan = (* Gérer exception fin de fichier *)
	input_char chan

let init_grid_size chan =
	grid_size := int_of_char' (read_char chan);
	ignore (input_line chan)

let rec string_of_piece_liste l =
	match l with
	| [] -> ()
	| p::t ->	let north = p.north in
				let south = p.south in
				let east  = p.east in
				let west  = p.west in
					if debug then begin
						Printf.printf "              |  |%i|  |\n" north;
						Printf.printf "Piece (%i,%i) : |%i |-| %i|\n" p.x p.y west east;
						Printf.printf "              |  |%i|  |\n\n" south
					end;
					string_of_piece_liste t

let rec init_grid_piece chan =
  	try
		for i = 0 to !grid_size - 1 do
  			for j = 0 to !grid_size - 1 do
				let line = input_line chan in
					let north = int_of_char' (String.get line 2) in
					let south = int_of_char' (String.get line 0) in
					let east  = int_of_char' (String.get line 3) in
					let west  = int_of_char' (String.get line 1) in
				ignore( add_piece { x=i; y=j; north=north; south=south; east=east; west=west } intGrid );
			done;
		done
	with
	| End_of_file -> ()

(* Met à jour la taille de la grille et la rempli *)
let init filename =
	let chan = open_in filename in
		try
			init_grid_size chan;
			init_grid_piece chan;
			string_of_piece_liste intGrid.pieces
	  	with
	  	| End_of_file -> ()

(* Renvoi le nombre de pièces du fichier ou 0 si le fichier est invalide *)
let rec nb_pieces chan acc =
	try
		let line = input_line chan in
			if String.get line 0 = '#' then
				nb_pieces chan acc
			else if (String.length line) <> 4 then begin
				error_msg := String.concat "" [!error_msg; (Printf.sprintf ("\t- chiffre(s) manquant(s) à la ligne %i\n") (acc + 2))];
				-1
			end
			else nb_pieces chan (acc + 1)
	with
	| End_of_file -> acc


(* Traite le fichier et renvoi true s'il est valide, false sinon *)
let parse_file filename =
	let chan = open_in filename in
		let size = (int_of_char' (read_char chan)) in
			let _ = input_line chan in (* Fini de lire la première ligne *)
				let nb = (nb_pieces chan 0) in
					if nb = 0 then begin
						error_msg := String.concat "" [!error_msg; "\t- Fichier vide\n"];
						false
					end
					else if nb = -1 then
						false
					else if (size * size) <> nb then begin
						error_msg := String.concat "" [!error_msg; "\t- Taille de la grille incorrect\n"];
						false
					end
					else
						true