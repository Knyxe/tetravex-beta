(* TETRAVEX *)
open Graphics
open Graph_tetra

type piece = { mutable x : int; mutable y : int; north : int; south: int; east : int; west: int }
type cell = Null | C of piece
type grille = { mutable pieces : piece list; mutable gx : int; mutable gy : int}

(* Grids *)
let intGrid = { pieces=[]; gx=0; gy=0 }
let int_grid_color = rgb 0 153 0

let finGrid = { pieces=[]; gx=0; gy=0 }
let fin_grid_color = rgb 204 102 0

let clickedPiece = ref Null

let make_matrix n =
	Array.make_matrix n n {x=-1; y=-1; north=-1; south=-1; east=-1; west=-1}

let array_of_grid g =
	List.fold_left (fun arr p -> arr.(p.x).(p.y) <- p) (make_matrix !grid_size) g.pieces

let copy_piece p =
	{x=p.x; y=p.y; north=p.north; south=p.south; east=p.east; west=p.west}

ttét;
let copy_liste l =
	List.map (fun p -> copy_piece p) l

let is_empty g x y = (* is the cell (x,y) of the grid g empty ? *)
	not ( List.exists (fun gp -> gp.x = x && gp.y = y) g.pieces )

let north_of_cell c =
	match c with
	| Null -> -99
	| C(p) -> p.north
	
let south_of_cell c =
	match c with
	| Null -> -99
	| C(p) -> p.south
	
let east_of_cell c =
	match c with
	| Null -> -99
	| C(p) -> p.east
	
let west_of_cell c =
	match c with
	| Null -> -99
	| C(p) -> p.west

let valid_place p l =
	let prox = 
		(* acc -> (north, south, east, west) *)
		List.fold_left
			(fun (n,s,e,w) pk ->
				if pk.x = p.x && pk.y = p.y+1 then (*North*)
					(C(pk),s,e,w) else
				if pk.x = p.x+1 && pk.y = p.y then (*East*)
					(n,s,C(pk),w) else
				if pk.x = p.x-1 && pk.y = p.y then (*West*)
					(n,s,e,C(pk)) else
				if pk.x = p.x && pk.y = p.y-1 then (*South*)
					(n,C(pk),e,w)
				else (n,s,e,w)
			) (Null,Null,Null,Null) l in
	match prox with
	| (cn, cs, ce, cw) ->
			(cn = Null || south_of_cell(cn) = p.north) &&
			(cs = Null || north_of_cell(cs) = p.south) &&
			(ce = Null || west_of_cell(ce) = p.east) &&
			(cw = Null || east_of_cell(cw) = p.west)

let add_piece p g = (* Add the piece p to the grid g if it's not in it *)
	if is_empty g p.x p.y then begin
		g.pieces <- p::g.pieces;
		true
		end
	else false

let move_piece p g =
	if g = intGrid then
		add_piece p g else
	if is_empty g p.x p.y && valid_place p g.pieces then begin
		g.pieces <- p::g.pieces;
		true
		end
	else false 

let remove_piece p g = (* Remove the piece p from the grid g *)
	g.pieces <- List.filter (fun gp -> gp <> p) g.pieces

let other_gride g =
	if g = intGrid then finGrid
	else intGrid

let find_all_wpieces l west =
	List.find_all (fun p -> p.west = west) l

let find_all_spieces l south =
	List.find_all (fun p -> p.south = south) l

let rec all_unique l =
	match l with
	| [] -> true
	| p::t -> ( List.exists (fun p2 -> p = p2) t ) || all_unique t

