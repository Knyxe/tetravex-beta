(* Window spec *)
open Graphics
open Grid_tetra

let window_color = ref (rgb 25 50 75)
let wincol = ref 1

let right = ref 900
let up = ref 400

let bot_span = ref 25
let side_span = ref 25
let mid_span = ref 25

let cell_size = ref 150
let cell_center = ref (!cell_size / 2)
let grid_size = ref 3
let cote = ref (!cell_size * !grid_size)

let switch_wincol () =
	if !wincol = 1 then
		begin
		window_color := (rgb 25 50 75);
		wincol := 0
		end
	else
		begin
		(*window_color := white;*)
		wincol := 1
		end

let draw_text x y col str =
	moveto x y;
	set_color col;
	set_font("9x15bold");
	set_text_size 100;
	draw_string str

let color_of_int i =
	if i = 0 then (rgb 100 180 160) else
	if i = 1 then (rgb 10 20 30) else
	if i = 2 then (rgb 70 30 40) else
	if i = 3 then (rgb 10 10 50) else
	if i = 4 then (rgb 50 30 30) else
	if i = 5 then (rgb 10 100 100) else
	if i = 6 then (rgb 50 60 70) else
	if i = 7 then (rgb 40 70 80) else
	if i = 8 then (rgb 0 80 90) else
	if i = 9 then (rgb 90 90 100)
	else (rgb (100+i) (100-i) (100-i))

let rec draw_grid_shape x y nb_cell width color =
	if nb_cell > 0 then
		begin
		set_color color;
		set_line_width width;
		draw_rect (x + ((nb_cell - 1) * !cell_size)) y !cell_size !cote;
		draw_rect x (y + ((nb_cell - 1) * !cell_size)) !cote !cell_size;
		draw_grid_shape x y (nb_cell - 1) width color
		end

let draw_context () = (* Draw the background ans two empty grids *)
	set_color !window_color;
	fill_rect 0 0 !right !up;

	let cote_fond = !cote + 20 in
	set_color int_grid_color;
	fill_rect (intGrid.gx - 10) (intGrid.gy - 10) cote_fond cote_fond;
	
	set_color fin_grid_color;
	fill_rect (finGrid.gx - 10) (finGrid.gy - 10) cote_fond cote_fond;
	
	draw_grid_shape intGrid.gx intGrid.gy !grid_size 5 black;

	draw_grid_shape finGrid.gx finGrid.gy !grid_size 5 black

let draw_piece piece absx absy = (* Draw the piece p at the position (absx, absy) *)
		set_line_width 5;
		let center = ( (absx + !cell_center), (absy + !cell_center) ) in
		let cote = !cell_size - 4 in
		let demicote = cote / 2 in
		let span = (cote * 3) / 4 in
		(* dessine la pièce *)
			set_color (color_of_int piece.north);
			fill_poly [| (absx, (absy + cote)); ((absx + cote), (absy + cote)); center |];

			set_color (color_of_int piece.south);
			fill_poly [| (absx, absy); ((absx+cote), absy); center |];

			set_color (color_of_int piece.east);
			fill_poly [| ((absx+cote), (absy+cote)); ((absx+cote), absy); center |];

			set_color (color_of_int piece.west);
			fill_poly [| (absx, absy); (absx, (absy+cote)); center |];

		(* écrit les valeurs sur la pièce *)
			draw_text (absx + demicote) (absy + span ) white (string_of_int piece.north);
			draw_text (absx + demicote) (absy + (cote / 4)) white (string_of_int piece.south);
			draw_text (absx + span) (absy + demicote) white (string_of_int piece.east);
			draw_text (absx + (cote / 4)) (absy + demicote) white (string_of_int piece.west);

		(* trace les diagonales de la pièce *)
			set_color black;
			set_line_width 3;

			draw_poly [| (absx, (absy + cote)); ((absx + cote), (absy + cote)); center |];
			draw_poly [| (absx, absy); ((absx+cote), absy); center |];
			draw_poly [| ((absx+cote), (absy+cote)); ((absx+cote), absy); center |];
			draw_poly [| (absx, absy); (absx, (absy+cote)); center |]

let rec draw_all_pieces pieces shift =
	match pieces with
	| [] -> ()
	| piece::t ->	let absx = (!cell_size * piece.x) + shift + 2 in
					let absy = (piece.y * !cell_size) + !bot_span + 2 in
						draw_piece piece absx absy;
						draw_all_pieces t shift

let rec draw_grid g =
	draw_all_pieces g.pieces g.gx

let draw_clicked_piece () =
	let mp = mouse_pos () in let mx = fst mp in let my = snd mp in
		match !clickedPiece with
		| Null -> ()
		| C(pc) ->
			let absx = (mx - (!cell_size / 2)) in
			let absy = (my - (!cell_size / 2)) in
				draw_piece pc absx absy

let scale_lengths () =
	right := max (size_x ()) (size_y ());
	up := min (size_x ()) (size_y ());
	resize_window !right !up;
	bot_span := !up / 6;
	cell_size := (!up - (!bot_span * 2)) / !grid_size;
	cote := !cell_size * !grid_size;
	side_span := (!right - (2 * !cote)) / 6;
	mid_span := !right - (!side_span * 2) - (2 * !cote);
	cell_center := (!cell_size / 2);

	intGrid.gx <- (!side_span + !cote + !mid_span);
	intGrid.gy <- !bot_span;
	finGrid.gx <- !side_span;
	finGrid.gy <- !bot_span

let screen_resized () =
	if !right <> size_x () || !up <> size_y () then
		begin
		scale_lengths ();
		true
		end
	else false

let draw_screen () =
	draw_context ();
	draw_grid intGrid;
	draw_grid finGrid;
	draw_clicked_piece ();
	switch_wincol ();
	synchronize ()

let p_is_clicked p g = (* piece is clicked ? *)
	let mp = mouse_pos () in let mx = ((fst mp) - g.gx) / !cell_size in let my = ((snd mp) - !bot_span) / !cell_size in
		if ( (mx = p.x) && (my = p.y) ) then
			true
		else false

let grid_clicked () =
	let mp = mouse_pos () in let mx = fst mp in let my = snd mp in
	let cote = !cell_size * !grid_size in
		not ( 	mx <= finGrid.gx || mx >= (intGrid.gx + cote) || (*Click to the left of finGrid or to the right of intGrid*)
				my <= !bot_span || my >= (intGrid.gy + cote) || (*Click above or below the grids*)
				(mx >= (finGrid.gx + cote) && mx <= (intGrid.gx)) (*Click between the grids*)
			)

let which_grid_clicked () = (* Which grid is clicked ? *)
	let mx = fst (mouse_pos ()) in
	(*la fonction n'est appelée que si on a cliqué à l'intérieur d'une grille
		on sait que mouse x sera soit > ou < à intGrid.gx*)
		if ( mx > intGrid.gx ) then
			intGrid
		else
			finGrid

let rec update_piece l g =
	match l with
	| [] -> ()
	| p::t ->	if p_is_clicked p g then
					begin
					clickedPiece := C(p);
					remove_piece p g
					end
				else update_piece t g

let rec drop_back g =
	match !clickedPiece with
	| C(pc) ->
		ignore (add_piece pc g);
		clickedPiece := Null
	| Null -> ()

let rec drop_new fst_grid snd_g l =
	let mp = mouse_pos () in let mx = fst mp in let my = snd mp in
	let side_shift = snd_g.gx in let bot_shift = snd_g.gy in
	let x = (mx - side_shift) / !cell_size in
	let y = (my - bot_shift) / !cell_size		
		match !clickedPiece with
			| C(pc) ->
					let new_p = { x=x;y=y; north=pc.north; south=pc.south; east=pc.east; west=pc.west} in
						if move_piece new_p snd_g then
							clickedPiece := Null
						else drop_back fst_grid
			| Null -> ()

let drop fst_grid =
	if grid_clicked () then
		let g =
			if (which_grid_clicked ()) = fst_grid then
				fst_grid
			else other_gride fst_grid in
		drop_new fst_grid g fst_grid.pieces
	else drop_back fst_grid

let rec drag fst_grid = (*p: clicked piece*)
	draw_screen ();
	ignore( wait_next_event [Mouse_motion; Button_up] );
	if button_down () then
		begin
		drag fst_grid
		end
	else drop fst_grid