./clear.sh || clear
ocamlc -c grid_tetra.ml
ocamlc -c graph_tetra.ml
ocamlc -c parse_tetra.ml
ocamlc -c tetravex.ml
ocamlc graphics.cma grid_tetra.cmo graph_tetra.cmo parse_tetra.cmo tetravex.cmo -o tetravex
